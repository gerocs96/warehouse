﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Warehouse.Migrations
{
    public partial class updateLogs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Column",
                table: "Logs");

            migrationBuilder.AddColumn<int>(
                name: "ItemId",
                table: "Logs",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ItemId",
                table: "Logs");

            migrationBuilder.AddColumn<string>(
                name: "Column",
                table: "Logs",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
