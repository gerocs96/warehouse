﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Warehouse.Models
{
    public class Delivery
    {
        [Key]
        [DisplayName("Azonosító")]
        public int Id { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Nem lehet negatív.")]
        [DisplayName("Termék azonosító")]
        public int ItemId { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Nem lehet negatív.")]
        [DisplayName("Darabszám")]
        public int Quantity { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Nem lehet negatív.")]
        [DisplayName("Forrás azonosító")]
        public int FromAddressId { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Nem lehet negatív.")]
        [DisplayName("Cél azonosító")]
        public int ToAddressId { get; set; }

        [DisplayName("Szállítás kezdete")]
        public DateTime Start { get; set; }

        [DisplayName("Megérkezett")]
        public bool HasArrived { get; set; }

        [DisplayName("Megjegyzés")]
        public string Comment { get; set; }
    }
}
