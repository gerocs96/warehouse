﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Warehouse.Models
{
    public class Log
    {
        [Key]
        [DisplayName("Azonosító")]
        public int Id { get; set; }

        [DisplayName("Tábla név")]
        public string TableName { get; set; }

        [DisplayName("Temék azonosító")]
        public int ItemId { get; set; }

        [DisplayName("Változtatás utáni érték")]
        public string ToValue { get; set; }

        [DisplayName("Dátum")]
        public DateTime Date { get; set; }
    }
}
