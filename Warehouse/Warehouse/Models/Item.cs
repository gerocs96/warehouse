﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Warehouse.Models
{
    public class Item
    {
        [Key]
        [DisplayName("Azonosító")]
        public int Id { get; set; }

        [DisplayName("Raktár azonosító")]
        public int WarehouseId { get; set; }

        [Required]
        [DisplayName("Cikkszám")]
        public string Number { get; set; }

        [Required]
        [DisplayName("Megnevezés")]
        public string Name { get; set; }

        [Required]
        [Range(1,int.MaxValue, ErrorMessage = "Nem lehet negatív.")]
        [DisplayName("Egységár")]
        public int Price { get; set; }

        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Nem lehet negatív.")]
        [DisplayName("Szabad készleten")]
        public int InStock { get; set; }

        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Nem lehet negatív.")]
        [DisplayName("Foglalt készleten")]
        public int Booked { get; set; }

        [DisplayName("Nem elérhető")]
        public bool Deleted { get; set; }
    }
}
