﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Warehouse.Models
{
    public class User
    {
        [Key]
        [DisplayName("Azonosító")]
        public int Id { get; set; }

        [DisplayName("Név")]
        public string Name { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Jelszó")]
        public string Password { get; set; }

        [DisplayName("Szerepkör")]
        public string JobTitle { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Nem lehet negatív.")]
        [DisplayName("Raktár azonosító")]
        public int WarehouseId { get; set; }

        [DisplayName("Beléphet")]
        public bool CanLogin { get; set; }
    }
}
