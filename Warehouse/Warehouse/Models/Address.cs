﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Warehouse.Models
{
    public class Address
    {   
        [Key]
        [DisplayName("Azonosító")]
        public int Id { get; set; }

        [DisplayName("Cím")]
        public string City { get; set; }
    }
}
