﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Warehouse.Data;
using Warehouse.Models;

namespace Warehouse.Services
{
    public class SecurityService
    {
        UsersDAO ud = new UsersDAO();
        private readonly ApplicationDbContext _db;

        public SecurityService(ApplicationDbContext db)
        {
            _db = db;
        }

        public SecurityService()
        {

        }

        public bool IsValid(User user)
        {
            if (ud.FindUser(user) == null)
            {
                return false;
            }
            return true;
        }
    }
}
