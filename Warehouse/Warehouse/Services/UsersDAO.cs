﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Warehouse.Models;

namespace Warehouse.Services
{
    public class UsersDAO
    {
        bool success = false;
        string connectionString = @"Server=DESKTOP-HL768KH; Database=WarehouseDb;Trusted_Connection=True; MultipleActiveResultSets=True";
    
        public User FindUser(User user)
        {
            string sqlStatement = "SELECT * FROM dbo.Users WHERE email = @email  AND password = @password ";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(sqlStatement, connection);

                command.Parameters.Add("@email", System.Data.SqlDbType.VarChar).Value = user.Email;
                command.Parameters.Add("@password", System.Data.SqlDbType.VarChar).Value = Utils.HashPassword(user.Password);

                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        success = true;
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            if (!success)
            {
                User error = new User();
                return error;
            }
            return user;
        }
    }
}
