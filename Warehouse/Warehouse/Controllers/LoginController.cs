﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Warehouse.Data;
using Warehouse.Models;
using Warehouse.Services;

namespace Warehouse.Controllers
{
    public class LoginController : Controller
    {
        private readonly ApplicationDbContext _db;

        public LoginController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ProcessLogin(User user)            
        {
            SecurityService ss = new SecurityService();

            foreach (var item in _db.Users)
            {
                if (user.Email == item.Email && Utils.HashPassword(user.Password) == item.Password)
                {
                    user = item;
                    //return View("LoginSuccess", user);
                    return RedirectToAction("Index", "Home");
                }
            }
            return View("LoginFailure", user);
        }
    }
}
