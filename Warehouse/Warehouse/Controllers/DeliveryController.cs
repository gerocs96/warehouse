﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Warehouse.Data;
using Warehouse.Models;

namespace Warehouse.Controllers
{
    public class DeliveryController : Controller
    {
        private readonly ApplicationDbContext _db;

        public DeliveryController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            IEnumerable<Delivery> objList = _db.Deliveries;
            return View(objList);
        }

        //get
        public IActionResult Create()
        {
            return View();
        }

        //post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Delivery obj)
        {
            if (ModelState.IsValid)
            {
                _db.Deliveries.Add(obj);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(obj);
        }

        //get delete
        public IActionResult Delete(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var obj = _db.Deliveries.Find(id);
            if (obj == null)
            {
                return NotFound();
            }
            return View(obj);
        }


        //post delete
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePost(int? id)
        {
            var obj = _db.Deliveries.Find(id);
            if (obj == null)
            {
                return NotFound();
            }
            _db.Deliveries.Remove(obj);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }        
    }
}
